package com.example.lucia.segvideo.functionalities.segmentData;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.lucia.segvideo.R;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.util.Log;

import com.example.lucia.segvideo.R;
/**
 * Created by lucia on 10/1/17.
 */

public class SegmentActivity extends AppCompatActivity {


    private String projectName;
    private String algorithm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_segments);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if(bundle != null){
            projectName = bundle.getString("name");
            algorithm = bundle.getString("algorithm");
        }

        Log.w("Project Name", projectName);
        Log.w("Choose Algortihm", algorithm);

    }
}
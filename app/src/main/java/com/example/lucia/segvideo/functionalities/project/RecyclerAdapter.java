package com.example.lucia.segvideo.functionalities.project;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;
import android.content.Intent;


import com.example.lucia.segvideo.R;
import com.example.lucia.segvideo.functionalities.algorithm.AlgorithmActivity;
import com.example.lucia.segvideo.functionalities.data.project.Project;
import com.example.lucia.segvideo.functionalities.projectMenu.ProjectMenu;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by lucia on 10/6/17.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerViewHolder> {

    private List<Project> mListProject = new ArrayList<>();
    private Context context;


    public RecyclerAdapter(Context context, List<Project> listProject){
        this.context = context;
        this.mListProject = listProject;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View row = inflater.inflate(R.layout.list_item_project, parent, false);
//        Item item = new Item(row);
        return new RecyclerViewHolder(row) ;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
         final String nameProject = mListProject.get(position).getName();
         int positionProject = mListProject.get(position).getId();
        String pos = Integer.toString(positionProject);

        holder.txt_name.setText(nameProject);
        holder.txt_position.setText(pos + ":");

        holder.setItemClickListener(new ItemClickListener(){


            @Override
            public void onClick(View view, int position, boolean inLongClick) {
//                if (inLongClick){
//                    ProjectActivity.viewProject(mListProject.get(position));
//                    Toast.makeText(context, "Long click:"+ mListProject.get(position),Toast.LENGTH_SHORT).show();
//                    Intent intent = new Intent(context, ProjectMenu.class);
//                    context.startActivity(intent);
//                } else {
                    Intent intent = new Intent(context, AlgorithmActivity.class);
                    intent.putExtra("name", mListProject.get(position).getName());
                    Toast.makeText(context, " "+ nameProject,Toast.LENGTH_SHORT).show();
                    context.startActivity(intent);


//                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mListProject.size();
    }
}

package com.example.lucia.segvideo.functionalities.base;

import android.view.View;

/**
 * Created by lucia on 10/1/17.
 */

public interface BaseView<T> {

        void loadUIElements(View view);
        void setUIEvents();
        void setPresenter(T presenter);
        boolean isActive();
}

